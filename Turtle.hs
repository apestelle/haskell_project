import TDA
import Prelude

data World = World Turtle Frame deriving (Show)
data Turtle = Turtle Float Float Bool Float deriving (Show)

data Order = Fwd Float
            |Bckwrd Float
            |Trn Float
            |RaisePen
            |LowerPen 
            |RepeatOrder [Order] Integer deriving(Show)

turn :: World -> Float -> World
turn (World (Turtle x y isWriting angle) frame) rotation = (World (Turtle x y isWriting (angle + rotation)) frame)

forward :: World -> Float -> World
forward (World (Turtle x y isWriting angle) frame) length =
    let
        x2 = x + length * sin(angle)
        y2 = y + length * cos(angle)
    in
    if isWriting then (World (Turtle x2 y2 isWriting angle) (addLineToDocument frame (Line x y x2 y2 "black" "black")))
    else (World (Turtle x2 y2 isWriting angle) frame)

backward :: World -> Float -> World
backward (World (Turtle x y isWriting angle) frame) length =
    let
        x2 = x - length * sin(angle)
        y2 = y - length * cos(angle)
    in
    if isWriting then (World (Turtle x2 y2 isWriting angle) (addLineToDocument frame (Line x y x2 y2 "black" "black")))
    else (World (Turtle x2 y2 isWriting angle) frame)

raisePen :: World -> World
raisePen (World (Turtle x y isWriting angle) frame) =
    (World (Turtle x y False angle) frame)

lowerPen :: World -> World
lowerPen (World (Turtle x y isWriting angle) frame) =
    (World (Turtle x y True angle) frame)

drawSquare :: World -> Float -> World
drawSquare world length =
    (turn (forward (turn (forward (turn (forward (turn (forward (lowerPen world) length) (-pi/2)) length) (-pi/2)) length) (-pi/2)) length) (-pi/2))

drawRegularPolygon :: World -> Float -> Float -> World
drawRegularPolygon world tessellation sideSize =
    drawRegularPolygonInternal world tessellation sideSize ((2*pi) / tessellation) tessellation
    where
        drawRegularPolygonInternal :: World -> Float -> Float -> Float -> Float -> World
        drawRegularPolygonInternal world tessellation sideSize angle 1 = (forward (turn world angle) sideSize)
        drawRegularPolygonInternal world tessellation sideSize angle i = (forward (turn (drawRegularPolygonInternal world tessellation sideSize angle (i - 1)) angle) sideSize)

drawBranch :: World -> Float -> World
drawBranch world squareSize = (backward (drawSquare (forward world squareSize) squareSize) squareSize)

drawBranches :: World -> Float -> Int -> World
drawBranches world squareSize 1 = drawBranch world squareSize
drawBranches world squareSize counter = drawBranch (turn (drawBranches world squareSize (counter - 1)) (pi/2)) squareSize

drawMill :: World -> Float -> World
drawMill world squareSize = drawBranches (lowerPen (turn world (pi/4))) squareSize 4

vonKochLine :: World -> Float -> Float -> World
vonKochLine world sideSize 0 = forward world sideSize
vonKochLine world sideSize generation =
    (vonKochLine (
        turn (
            vonKochLine (
                turn (
                    (vonKochLine (
                        turn (
                            vonKochLine world nextSideSize nextGeneration
                        ) sixtyDegRight
                    ) nextSideSize nextGeneration)
                ) hundredTwentyDegLeft
            ) nextSideSize nextGeneration
        ) sixtyDegRight
    ) nextSideSize nextGeneration)
    where
        nextGeneration = (generation - 1)
        nextSideSize = (sideSize / 3)
        sixtyDegRight = (pi / 3)
        hundredTwentyDegLeft = (-2 * pi / 3)

drawSnowFlake :: World -> Float -> Float -> World
drawSnowFlake world sideSize generationsCount =
    (vonKochLine (
        turn (
            vonKochLine (
                turn (
                   vonKochLine world sideSize generationsCount
                ) hundredTwentyDegLeft
            ) sideSize generationsCount
        ) hundredTwentyDegLeft
    ) sideSize generationsCount)
    where
        hundredTwentyDegLeft = (-2 * pi / 3)

exportFrameFromWorld (World _ frame) = exportFrameToString frame

-- Fwd Float
-- Bckwrd Float
-- Trn Float
-- RaisePen
-- LowerPen deriving(Show)

repeatOrder world _ 0 = world
repeatOrder world [] _ = world
repeatOrder world orders iteration =
    repeatOrder (executeOrders orders world) orders (iteration - 1)

executeOrders [] world = world
executeOrders (h:t) world =
    executeOrders t (executeOrder h world)

executeOrder (Fwd length) world =
    forward world length
executeOrder (Trn length) world =
    turn world length
executeOrder (Bckwrd length) world =
    turn world length
executeOrder (RaisePen) world =
    raisePen world
executeOrder (LowerPen) world =
    lowerPen world

executeOrder (RepeatOrder orders iteration) world =
    repeatOrder world orders iteration

executeProgram path world orders  = 
    writeFile path (exportFrameFromWorld (executeOrders orders world))

mainTestTurtle = let
    maxWidth = 1920
    maxHeight = 969
    world = (World (Turtle (1920 / 2) (969 / 2) True 0) (Frame maxWidth maxHeight []))
    fwd = (Fwd 50)
    trn = (Trn (pi/2))
    in do
        print $ world
        writeFile "svg/world.svg" (exportFrameFromWorld (forward (lowerPen (forward (raisePen (backward (turn (forward (turn (forward world 50) (pi/4)) 50) (pi/3)) 20)) 30)) 20))
        writeFile "svg/squareWorld.svg" (exportFrameFromWorld (drawSquare world 150))

        writeFile "svg/regularPolygon10.svg" (exportFrameFromWorld (drawRegularPolygon world 10 50))
        writeFile "svg/regularPolygon100.svg" (exportFrameFromWorld (drawRegularPolygon world 100 5))
        writeFile "svg/regularPolygon1000.svg" (exportFrameFromWorld (drawRegularPolygon world 1000 1))

        writeFile "svg/mill.svg" (exportFrameFromWorld (drawMill world 50))

        writeFile "svg/snowflake1.svg" (exportFrameFromWorld (drawSnowFlake world 100 1))
        writeFile "svg/snowflake2.svg" (exportFrameFromWorld (drawSnowFlake world 100 2))
        writeFile "svg/snowflake3.svg" (exportFrameFromWorld (drawSnowFlake world 100 3))
        writeFile "svg/snowflake4.svg" (exportFrameFromWorld (drawSnowFlake world 100 4))
        writeFile "svg/snowflake5.svg" (exportFrameFromWorld (drawSnowFlake world 100 5))

        writeFile "svg/turtleLanguage1.svg" (exportFrameFromWorld (repeatOrder world [ fwd, trn, fwd, trn, fwd, trn, fwd, trn ] 1))
        writeFile "svg/turtleLanguage2.svg" (exportFrameFromWorld (repeatOrder world [ fwd, trn ] 4))
        executeProgram "svg/turtleLanguage3.svg" world [ (RepeatOrder [ fwd, trn ] 4) ]