module TDA (
    Frame(Frame),
    exportFrameToString,
    generateRandomSquares,
    addLineToDocument,
    Shape (Rect, Circle, Line, Polyline),
)
where

import System.Random
import Numeric (showHex)

data Frame = Frame Int Int [Shape] deriving (Show) 

data Shape = Rect Int Int Int Int String String
            | Circle Int Int Int String String 
            | Line Float Float Float Float String String  
            | Polyline String String String deriving (Show) 

addLineToDocument (Frame width height shapes) shape = (Frame width height (shapes ++ [shape]))

exportFrameToString :: Frame -> [Char]
exportFrameToString (Frame width height shapes) =
    "<?xml version=\"1.0\" standalone=\"no\"?>\n" ++ "<svg width=\""++ (show width) ++"\" height=\""++ (show height) ++"\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n"
         ++ (exportShapesToString shapes) ++ 
    "</svg>"

exportShapesToString :: [Shape] -> [Char]
exportShapesToString [] = ""
exportShapesToString (h:t) = 
    (exportShapeToString h) ++ (exportShapesToString t)

exportShapeToString :: Shape -> String
exportShapeToString (Rect x y width height stroke fill) =
    "\t<rect x=\""++ (show x) ++"\" y=\""++ (show y) ++"\" width=\""++ (show width) ++"\" height=\""++ (show height) ++"\" stroke=\""++ stroke ++"\" fill=\""++ fill ++"\" stroke-width=\"2\"/>\n"

exportShapeToString (Circle cx cy r stroke fill) =
    "\t<circle cx=\""++ (show cx) ++"\" cy=\""++ (show cy) ++"\" r=\""++ (show r) ++"\" stroke=\""++ stroke ++"\" fill=\""++ fill ++"\" stroke-width=\"2\"/>\n"

exportShapeToString (Line x1 y1 x2 y2 stroke fill) =
    "\t<line x1=\""++ (show x1) ++"\" x2=\""++ (show x2) ++"\" y1=\""++ (show y1) ++"\" y2=\""++ (show y2) ++"\" stroke=\""++ stroke ++"\" fill=\""++ fill ++"\" stroke-width=\"2\"/>\n"

exportShapeToString (Polyline points stroke fill) =
    "\t<polyline points=\""++ points ++"\" stroke=\""++ stroke ++"\" fill=\""++ fill ++"\" stroke-width=\"2\"/>\n"


generateRandomSquares :: Int -> Int -> Int -> StdGen -> [Shape];
generateRandomSquares 0 _ _ _= []    
generateRandomSquares counter maxWidth maxHeight seed = do
    let (x, seed2) = randomR (0, maxWidth :: Int) seed
    let (y, seed3) = randomR (0, maxHeight :: Int) seed2
    let (width, seed4) = randomR (0, (maxWidth - x)  :: Int) seed3
    let (height, seed5) = randomR (0, (maxHeight - y)  :: Int) seed4
    let (color, seed6) = randomR (0, 16777216 :: Int) seed5
    let (fill, seed7) = randomR (0, 16777216 :: Int) seed6

    (Rect x y width height ("#"++ (showHex color "")) ("#"++ (showHex fill ""))) :(generateRandomSquares (counter - 1) maxWidth maxHeight seed7)

mainTestImage = let 
    maxWidth = 1920
    maxHeight = 969
    frame = (Frame maxWidth maxHeight 
        [
            (Rect 10 10 50 30 "black" "transparent"),
            (Circle 25 75 20 "red" "transparent"),
            (Line 10 50 110 150 "orange" "transparent"),
            (Polyline "10 210 15 220 20 215 25 230 30 225 35 240 40 235 45 250" "rgb(100,29,60)" "transparent")
        ])
    seed = mkStdGen 42

    frame2 = (Frame maxWidth maxHeight (generateRandomSquares 100000 maxWidth maxHeight seed))
    in do
        print $ frame
        print $ exportFrameToString frame
        writeFile "svg/output.svg" (exportFrameToString frame)
        print $ exportFrameToString frame2
        writeFile "svg/randomSquares.svg" (exportFrameToString frame2)